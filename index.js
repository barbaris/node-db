const verbosity = require('barbaris-verbosity');
const sqlite3 = require('sqlite3').verbose();
const {dbConfig} = require('barbaris-config')
const fs = require('fs');
const rootDir = require('path').dirname(require.main.filename);

let db = new sqlite3.Database(dbConfig.database, sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, (err) => {
    if (err) {
        console.error(err.message);
    } else {
        if (verbosity > 0) console.log('Connected to the SmartHouse database:', dbConfig.database);
        loadMigrations();
    }
});

wrapper = {
    all: (sql, args) => {
        return new Promise((resolve, reject) => {
            db.all(sql, args, (err, rows) => {
                if (err) return reject(err);
                resolve(rows);
            })
        })
    },
    one: (sql, args) => {
        return new Promise((resolve, reject) => {
            db.get(sql, args, (err, row) => {
                if (err) return reject(err);
                resolve(row);
            })
        })
    },
    insert: (sql, args) => {
        return new Promise((resolve, reject) => {
            db.run(sql, args, (err) => {
                if (err) return reject(err);
                db.get('select last_insert_rowid() as lastId', (err, data) => {
                    if (err) return reject(err);
                    resolve(data.lastId);
                })
            })
        })
    },
    exec: (sql, args) => {
        return new Promise((resolve, reject) => {
            db.run(sql, args, (err) => {
                if (err) return reject(err);
                resolve();
            })
        })
    }
};

const loadMigrations = async () => {
    if (verbosity >= 1) console.log('Loading migrations');
    const files = fs.readdirSync(rootDir + '/migrations');
    for (let file in files) {
        const migration = files[file].split('.')[0];
        await wrapper.exec('create table if not exists migrations (migration CHAR(12) primary key);')
        const w = await wrapper.all('select * from migrations where migration = ?', [migration]);
        if (w.length < 1) {
            if (verbosity >= 2) console.log('Migrating ' + migration);
            const sql = fs.readFileSync(rootDir + '/migrations/' + files[file], "UTF-8");
            if (verbosity >= 3) console.log(sql);
            await wrapper.exec(sql);
            await wrapper.exec('INSERT INTO migrations VALUES (?)', [migration]);
            if (verbosity >= 2) console.log('OK');
        }
    }
};

module.exports = wrapper;
